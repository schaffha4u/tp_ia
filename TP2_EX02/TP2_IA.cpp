// TP2_IA.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include "Sommet.h"
#include "Edge.h"
#include <deque>
#include <iterator>
#include <algorithm>
using namespace std;

deque<Sommet*>* OUVERT, * FERME;
vector<Sommet*> graphe;

void afficherSolution(Sommet* n) {
    vector<Sommet*> vec;
    vec.push_back(n);
    Sommet* tmp = n->pere;
    while (tmp->pere != NULL) {
        vec.push_back(tmp);
        tmp = tmp->pere;
    }

    reverse(vec.begin(), vec.end());


    cout << "Noeud but " << n->name << " trouve, le parcours est donc termine!" << endl;
    cout << endl << "Solution finale : " << tmp->name;
    for (auto it : vec)
        cout << ", " << it->name;
    cout << endl;
}

void trierOUVERT_BFS() {
    std::sort(OUVERT->begin(), OUVERT->end(), [](Sommet* a, Sommet* b) {
        return a->h < b->h;
        });
}

void afficherOF_BFS() {
    cout << "OUVERT : ";
    for (auto it : *OUVERT) {
        cout << "(" << it->name << ", h=" << it->h << ") / ";
    }
    cout << endl;
    cout << "FERME : ";
    for (auto it : *FERME) {
        cout << "(" << it->name << ", " << it->h << ") / ";
    }
    cout << endl << "------------------------------------------------------------------------"<< endl << endl;
}

void trierOUVERT_BAB() {
    std::sort(OUVERT->begin(), OUVERT->end(), [](Sommet* a, Sommet* b) {
        return a->g < b->g;
        });
}

void afficherOF_BAB() {
    cout << "OUVERT : ";
    for (auto it : *OUVERT) {
        cout << "(" << it->name << ", g=" << it->g << ") / ";
    }
    cout << endl;
    cout << "FERME : ";
    for (auto it : *FERME) {
        cout << "(" << it->name << ", " << it->g << ") / ";
    }
    cout << endl << "------------------------------------------------------------------------" << endl << endl;
}

void trierOUVERT_AS() {
    std::sort(OUVERT->begin(), OUVERT->end(), [](Sommet* a, Sommet* b) {
        return a->f < b->f;
        });
}

void afficherOF_AS() {
    cout << "OUVERT : ";
    for (auto it : *OUVERT) {
        cout << "(" << it->name << ", f=" << it->f <<",h="<< it->h << "g="<< it->g << ") / ";
    }
    cout << endl;
    cout << "FERME : ";
    for (auto it : *FERME) {
        cout << "(" << it->name << ", " << it->f
            << ") / ";
    }
    cout << endl << "------------------------------------------------------------------------" << endl << endl;
}

bool best_first_search(Sommet* depart, vector<Sommet*> buts) {
    OUVERT = new deque<Sommet*>();
    FERME = new deque<Sommet*>();
    for (auto it : graphe) {
        it->setPere(NULL);
    }

    OUVERT->push_back(depart);
    afficherOF_BFS();
    /////////////////////////////////////////////////
    while (OUVERT->size() > 0) {
        Sommet* n = OUVERT->front();
        OUVERT->pop_front();
        FERME->push_back(n);
        for (auto it : n->_liste_edge_sortant) {
            Sommet* n_prime = it->getOther(n);
            if (find(buts.begin(), buts.end(), n_prime) != buts.end()) {//Si on trouve un noeud but
                n_prime->setPere(n);
                OUVERT->push_back(n_prime);
                trierOUVERT_BFS();
                afficherOF_BFS();
                afficherSolution(n_prime);
                return true;
            }
            else if (find(OUVERT->begin(), OUVERT->end(), n_prime) == OUVERT->end() && find(FERME->begin(), FERME->end(), n_prime) == FERME->end()) {//Si n' ni dans OUVERT ni dans FERME
                n_prime->setPere(n);
                OUVERT->push_back(n_prime);
            }

            trierOUVERT_BFS();
        }
        afficherOF_BFS();
    }
    ////////////////////////////////////////
    delete(OUVERT);
    delete(FERME);
    return false;
}

bool branch_and_bound(Sommet* depart, vector<Sommet*> buts) {
    OUVERT = new deque<Sommet*>();
    FERME = new deque<Sommet*>();
    for (auto it : graphe) {
        it->setPere(NULL);
        it->g = 0;
        it->f = 0;
    }
    depart->setG(0);
    OUVERT->push_back(depart);
    afficherOF_BAB();
    /////////////////////////////////////////
    while (OUVERT->size() > 0) {
        Sommet* n = OUVERT->front();
        OUVERT->pop_front();
        FERME->push_back(n);
        for (auto it : n->_liste_edge_sortant) {
            Sommet* n_prime = it->getOther(n);
            if (find(buts.begin(), buts.end(), n_prime) != buts.end()) {//Si on trouve un noeud but
                n_prime->setPere(n);
                n_prime->g = n->g + it->getVal();
                OUVERT->push_back(n_prime);
                trierOUVERT_BAB();
                afficherOF_BAB();
                afficherSolution(n_prime);
                return true;
            }
            else if (find(OUVERT->begin(), OUVERT->end(), n_prime) == OUVERT->end() && find(FERME->begin(), FERME->end(), n_prime) == FERME->end()) {//Si n' ni dans OUVERT ni dans FERME
                n_prime->setPere(n);
                n_prime->g = n->g + it->getVal();
                OUVERT->push_back(n_prime);
            }
            else if(n_prime->g > n->g + it->getVal()){
                n_prime->setPere(n);
                n_prime->g = n->g + it->getVal();
                if (find(FERME->begin(), FERME->end(), n_prime) != FERME->end()) {
                    auto i = find(FERME->begin(), FERME->end(), n_prime);
                    OUVERT->push_back(*i);
                    FERME->erase(i);
                }
            }

            trierOUVERT_BAB();
        }
        afficherOF_BAB();
    }


    ////////////////////////////////////////
    delete(OUVERT);
    delete(FERME);
    return false;

}

//Applique l'algorithme A* au graphe graph, en partant du sommet depart jusqu'au sommet endd
//f(n') = g(n') + h(n') avec g(n') = g(n) + C(n, n');
//La fonction coût est la distance parcourue au niveau des arêtes
//La fonction heuristique est la distance dans le plan qu'il reste à parcourir pour arriver au sommet end
bool algo_a_star(Sommet* depart, vector<Sommet*> buts) {
    OUVERT = new deque<Sommet*>();
    FERME = new deque<Sommet*>();
    for (auto it : graphe) {
        it->setPere(NULL);
        it->setG(0);
        it->f = 0;
    }

    OUVERT->push_back(depart);
    depart->setF(depart->getH());
    afficherOF_AS();
    while (OUVERT->size() > 0) {
        Sommet* n = OUVERT->front();
        OUVERT->pop_front();
        FERME->push_back(n);

        // Pour tous les voisins n_prime de n
            //(On parcourt la liste d'adjacence de n)
        for (auto edge : n->_liste_edge_sortant) {
            Sommet* n_prime = edge->getOther(n);
            Sommet* n_prime_temp = new Sommet(n_prime);
            n_prime_temp->setPere(n);
            n_prime_temp->setG(n->getG() + edge->getVal());
            n_prime_temp->setF(n_prime_temp->getH() + n_prime_temp->getG());
            if (find(buts.begin(), buts.end(), n_prime) != buts.end()) {
                n_prime->setF(n_prime_temp->getF());
                n_prime->setG(n_prime_temp->getG());
                n_prime->setPere(n);
                OUVERT->push_back(n_prime);
                trierOUVERT_AS();
                afficherOF_AS();
                afficherSolution(n_prime);
                return true;
            }

            if (find(OUVERT->begin(), OUVERT->end(), n_prime) == OUVERT->end() && find(FERME->begin(), FERME->end(), n_prime) == FERME->end()) {//Si n' ni dans O  ni dans F
                n_prime->setF(n_prime_temp->getF());
                n_prime->setG(n_prime_temp->getG());
                n_prime->setPere(n_prime_temp->getPere());
                OUVERT->push_back(n_prime);
            }

            trierOUVERT_AS();
        }
        afficherOF_AS();
    }

    /////////////////////////
    delete(OUVERT);
    delete(FERME);
    return false;
}

int main()
{
    Sommet* A = new Sommet("A", 2);
    Sommet* B = new Sommet("B", 1);
    Sommet* C = new Sommet("C", 3);
    Sommet* D = new Sommet("D", 1);
    Sommet* E = new Sommet("E", 6);
    Sommet* S = new Sommet("S", 5);
    Sommet* G1 = new Sommet("G1", 0);
    Sommet* G2 = new Sommet("G2", 0);

    Edge* e1 = new Edge(S, A, 2);
    Edge* e2 = new Edge(S, C, 3);
    S->ajouter(e1);
    S->ajouter(e2);

    Edge* e3 = new Edge(A, B, 1);
    Edge* e4 = new Edge(A, E, 8);
    A->ajouter(e3);
    A->ajouter(e4);

    Edge* e5 = new Edge(B, S, 2);
    Edge* e6 = new Edge(B, C, 1);
    Edge* e7 = new Edge(B, D, 1);
    Edge* e8 = new Edge(B, G1, 4);
    B->ajouter(e5);
    B->ajouter(e6);
    B->ajouter(e7);
    B->ajouter(e8);

    Edge* e9 = new Edge(C, D, 1);
    Edge* e10 = new Edge(C, G2, 5);
    C->ajouter(e9);
    C->ajouter(e10);

    Edge* e11 = new Edge(D, G1, 5);
    Edge* e12 = new Edge(D, G2, 1);
    D->ajouter(e11);
    D->ajouter(e12);

    Edge* e13 = new Edge(E, G1, 9);
    Edge* e14 = new Edge(E, G2, 7);
    E->ajouter(e13);
    E->ajouter(e14);

    Edge* e15 = new Edge(G1, S, 4);
    G1->ajouter(e15);

    graphe.push_back(A);
    graphe.push_back(B);
    graphe.push_back(C);
    graphe.push_back(D);
    graphe.push_back(E);
    graphe.push_back(S);
    graphe.push_back(G1);
    graphe.push_back(G2);

    vector<Sommet*> buts{ G1, G2 };

    bool result = best_first_search(S, buts);
    if (!result)
        cout << "Noeud(s) but(s) non atteignable(s)." << endl;

    return 0;
}