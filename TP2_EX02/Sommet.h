#pragma once
#include <string>
#include <vector>

using namespace std;

class Edge;

class Sommet
{
public:
	int f, h, g;

	string name;
	vector<Edge*> _liste_edge_sortant;

	Sommet* pere;

	Sommet(string name, int h) {
		this->name = name;
		this->h = h;
	}

	Sommet(Sommet* s) {
		this->name = s->name;
		this->h = s->h;
		this->f = s->f;
		this->g = s->g;
	}

	void ajouter(Edge* e) {
		_liste_edge_sortant.push_back(e);
	}

	int getH() const { return h; }
	int getF() const { return f; }
	int getG() const { return g; }

	void setG(int v) {
		g = v;
	}

	void setH(int h) {
		this->h = h;
	}

	void setF(int f) {
		this->f = f;
	}

	void setPere(Sommet* p) {
		pere = p;
	}

	Sommet* getPere() { return pere; }
};

