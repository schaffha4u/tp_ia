#pragma once
#include "Sommet.h"

class Edge
{
public:
	Sommet* s1, * s2;
	int val;
	Edge(Sommet* s1, Sommet* s2, int val) {
		this->s1 = s1;
		this->s2 = s2;
		this->val = val;
	}

	int getVal() const { return val; }
	Sommet* getOther(Sommet* s) { if (s1 == s) return s2; else return s1; }
};

