#include "Sommet.h"

ostream& operator<<(ostream& s, const Sommet& so) {
	s << "Sommet " << so.getName() <<  ", f = " << so.getF() << endl;
	return s;
}