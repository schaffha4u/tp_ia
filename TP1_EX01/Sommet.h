#pragma once
#include <list>
#include "Edge.h"
#include <iostream>
#include <string>
using namespace std;

class Sommet
{
private:
	string name;
	std::list<Edge*> _adj_list;
	int x, y, z;
	int f = 0, g = 0, h = 0;
	Sommet* pere;
public:
	Sommet(int x, int y, int z, string name){
		this->x = x;
		this->y = y;
		this->z = z;
		this->name = name;
	}

	Sommet(const Sommet* s) {
		this->x = s->x;
		this->y = s->y;
		this->z = s->z;
		this->name = s->name;
		this->pere = s->pere;
		this->_adj_list = s->_adj_list;
	}

	void ajouter(Edge* e) {
		_adj_list.push_back(e);
	}

	std::list<Edge*> getAdjList() const {
		return _adj_list;
	}

	Sommet* getPere() const { return pere; }
	void setPere(Sommet* s) { this->pere = s; }

	int getX() const { return x; }
	int getY() const { return y; }
	int getZ() const { return z; }

	int getF() const { return f; }
	void setF(int F) { f = F; }

	int getG() const { return g; }
	void setG(int G) { g = G; }

	int getH() const { return h; }
	void setH(int H) { h = H; }

	string getName() const { return name; }

	friend ostream& operator<<(ostream&, const Sommet&);
};

extern ostream& operator<<(ostream&, const Sommet&);

