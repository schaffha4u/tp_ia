#pragma once
class Sommet;

class Edge
{
private:
	Sommet* a, *b;
public:
	Edge(Sommet* a, Sommet* b) {
		this->a = a;
		this->b = b;
	}

	Sommet* getA() {
		return a;
	}

	Sommet* getB() {
		return b;
	}

	//Retourne l'autre sommet de l'ar�te (diff�rent de s)
	Sommet* getEnd(Sommet* s) {
		return (s == a) ? b : a;
	}
};

