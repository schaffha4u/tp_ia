
#include <iostream>
#include "Sommet.h"
#include "Edge.h"
#include <list>
#include <deque>
#include <math.h>
#include <algorithm>
#include <vector>

using namespace std;

void afficherSolution(Sommet* n) {
    vector<Sommet*> vec;
    vec.push_back(n);
    Sommet* tmp = n->getPere();
    while (tmp->getPere() != NULL) {
        vec.push_back(tmp);
        tmp = tmp->getPere();
    }

    reverse(vec.begin(), vec.end());


    cout << "Noeud but " << n->getName() << " trouve, le parcours est donc termine!" << endl;
    cout << endl << "Solution finale : " << tmp->getName();
    for (auto it : vec)
        cout << ", " << it->getName();
    cout << endl;
}

//Utilisée pour trier la liste OUVERT avec le membre F de la classe Sommet
bool comp(const Sommet* a, const Sommet* b) {
    return a->getF() < b->getF();
}

//Retourne C(n, n')
int c(Sommet* n, Sommet* n_prime) {
    return abs(n_prime->getX() - n->getX()) + abs(n_prime->getY() - n->getY()) + abs(n_prime->getZ() - n->getZ());
}

//Retourne h(n')
//Distance dans le plan
double h(Sommet* s, Sommet* end) {
    int x_ = (end->getX() - s->getX()) * (end->getX() - s->getX());
    int y_ = (end->getY() - s->getY()) * (end->getY() - s->getY());
    int z_ = (end->getZ() - s->getZ()) * (end->getZ() - s->getZ());
    return sqrt(x_ + y_ + z_);
}

//Applique l'algorithme A* au graphe graph, en partant du sommet depart jusqu'au sommet endd
//f(n') = g(n') + h(n') avec g(n') = g(n) + C(n, n');
//La fonction coût est la distance parcourue au niveau des arêtes
//La fonction heuristique est la distance dans le plan qu'il reste à parcourir pour arriver au sommet end
bool algo_a_star(Sommet* depart, Sommet* end) {
    deque<Sommet*> O, F;
    O.push_back(depart);
    while (!O.empty()) {
        Sommet* n = O.front();
        O.pop_front();
        F.push_back(n);
        
        //Pour tous les voisins n_prime de n
        //(On parcourt la liste d'adjacence de n)
        for (auto edge : n->getAdjList()) {
            Sommet* n_prime = edge->getEnd(n);
            Sommet* n_prime_temp = new Sommet(edge->getEnd(n));
            n_prime_temp->setPere(n);
            n_prime_temp->setH(h(n_prime_temp, end));
            n_prime_temp->setG(n->getG() + c(n, n_prime_temp));
            n_prime_temp->setF(n_prime_temp->getH() + n_prime_temp->getG());
            if (n_prime_temp->getName().compare(end->getName()) == 0) {
                n_prime->setF(n_prime_temp->getF());
                n_prime->setG(n_prime_temp->getG());
                n_prime->setH(n_prime_temp->getH());
                n_prime->setPere(n);
                return true;
            }

            if (find(O.begin(), O.end(), n_prime) == O.end() && find(F.begin(), F.end(), n_prime) == F.end()) {//Si n' ni dans O  ni dans F
                n_prime->setF(n_prime_temp->getF());
                n_prime->setG(n_prime_temp->getG());
                n_prime->setH(n_prime_temp->getH());
                n_prime->setPere(n_prime_temp->getPere());
                O.push_back(n_prime);
                delete(n_prime_temp);
            }
            else {
                delete(n_prime_temp);
            }
            sort(O.begin(), O.end(), comp);
        }
    }

    return false;
}

int main()
{
    //Création des sommets et des arêtes
    Sommet* a = new Sommet(0, 0, 0, "a");
    Sommet* b = new Sommet(0, 1, 0, "b");
    Sommet* c = new Sommet(1, 1, 0, "c");
    Sommet* d = new Sommet(2, 0, 0, "d");
    Sommet* e = new Sommet(2, 2, 0, "e");
    Sommet* f = new Sommet(1, 2, 0, "f");
    Sommet* g = new Sommet(2, 2, 2, "g");
    Sommet* h = new Sommet(0, 2, 2, "h");
    Edge* e1 = new Edge(a, d);
    Edge* e2 = new Edge(a, b);
    Edge* e3 = new Edge(b, c);
    Edge* e4 = new Edge(c, f);
    Edge* e5 = new Edge(f, e);
    Edge* e6 = new Edge(e, d);
    Edge* e7 = new Edge(e, g);
    Edge* e8 = new Edge(g, h);

    //Création du graphe
    a->ajouter(e1); a->ajouter(e2);
    b->ajouter(e2); b->ajouter(e3);
    c->ajouter(e3); c->ajouter(e4);
    d->ajouter(e2); d->ajouter(e6);
    e->ajouter(e5); e->ajouter(e6); e->ajouter(e7);
    f->ajouter(e4); f->ajouter(e5);
    g->ajouter(e7); g->ajouter(e8);
    h->ajouter(e8);

    list<Sommet*> graph = { a, b, c, d, e, f, g, h };

    //Execution de l'algo
    bool test = algo_a_star(a, h);

    if (test)
        afficherSolution(h);
    else
        cout << "Sommet but non atteignable." << endl;
    return 0;
}