import java.util.ArrayDeque;
import java.util.Deque;

public class Tour {
	public Deque<Integer> _disques = new ArrayDeque<Integer>();
	public Tour(int n) {
		for (int i = 1; i <= n; i++) {
			_disques.addLast(i);
		}
	}
	
	public static void move(Tour t1, Tour t2) {
		int tmp = t1._disques.poll();
		t2._disques.addFirst(tmp);
	}
	
	public void afficher() {
		System.out.print("Tour :");
		for (int d : _disques) {
			System.out.print(d + " ");
		}
		System.out.println("");
	}
}
