import java.util.Scanner;

public class TourHanoi {
	private static FenetreDessin f;
	private static Tour t1, t2, t3;
	private static int speed;
	
	
	public static void hanoi(int n, Tour D, Tour A, Tour I) {
		if (n != 0) {
	        hanoi(n - 1, D, I, A);
	        Tour.move(D, A);

	        f.update(t1, t2, t3);
	        try {
				Thread.sleep(speed);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	        hanoi(n - 1, I, A, D);
	    }
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		final int n;
		
		System.out.println("Entrez le nombre de disques souhait�s :");
	    String tmp = scanner.nextLine();
	    n = Integer.parseInt(tmp);
	    System.out.println("Entrez la vitesse d'animation souhait�e (en ms) :");
	    tmp = scanner.nextLine();
	    speed = Integer.parseInt(tmp);
		
		t1 = new Tour(n);
		t2 = new Tour(0);
		t3 = new Tour(0);
		
		try {
			Thread.sleep(1200);
			f = new FenetreDessin(1000, 1700, "Hanoi", n);
			f.afficherBases();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		f.update(t1, t2, t3);
		hanoi(n, t1, t3, t2);
		f.update(t1, t2, t3);
		
		
	}

}
