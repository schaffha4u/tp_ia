

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import javax.swing.JFrame;
import java.util.Iterator;

public class FenetreDessin extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private BufferStrategy bufferStrat;
	private int x_t1, x_t2, x_t3, y_base, h_disque; //Nombre de disques et x central des colonnes
	private final int w_col = 20;
	
	public FenetreDessin(int h, int w, String titre, int n) throws InterruptedException{
		super(titre);
		this.setSize(w, h);
		this.setIgnoreRepaint(true);
		this.setVisible(true);
	
		x_t1 = 0;
		x_t2 = w/3;
		x_t3 = 2*w/3;
		x_t1 = (x_t1 + x_t2)/2;
		x_t2 = (x_t2 + x_t3)/2;
		x_t3 = (x_t3 + w)/2;
		y_base = h - 80;
		h_disque = 20;
		
		int nbBuffers = 1;
		createBufferStrategy(nbBuffers);
		
		Thread.sleep(150);
		
		this.bufferStrat = getBufferStrategy();
	}
	
	public void afficherBases() {
		Graphics2D g2 = initGraphicContext();
		g2.setColor(Color.BLACK);
		g2.fillRect(25, y_base, this.getWidth() - 50, 30);
		
		g2.fillRect(x_t1 - w_col/2, 50, w_col, y_base-50);
		g2.fillRect(x_t2 - w_col/2, 50, w_col, y_base-50);
		g2.fillRect(x_t3 - w_col/2, 50, w_col, y_base-50);
		
		printContext(g2);
	}
	
	public void update(Tour t1, Tour t2, Tour t3) {
		int cpt = 1;
		Graphics2D g2 = initGraphicContext();
		g2.clearRect(0, 0, getWidth(), getHeight());
		
		
		afficherBases();
		
		Iterator<Integer> it = t1._disques.descendingIterator();
		
        while (it.hasNext()) {
        	int val = it.next();
        	g2.setColor(new Color((val*100)%255, (val*10000)%255, (val*1000)%255));
            g2.fillRect(x_t1 - val*30, y_base - cpt * h_disque - ((cpt-1) * 5) - 5, val * 60, h_disque);
            cpt++;
        }

       cpt = 1;
       it = t2._disques.descendingIterator();
       while (it.hasNext()) {
       		int val = it.next();
       		g2.setColor(new Color((val*100)%255, (val*10000)%255, (val*1000)%255));
           g2.fillRect(x_t2 - val*30, y_base - cpt * h_disque - ((cpt-1) * 5) - 5, val * 60, h_disque);
           cpt++;
       }
       
       cpt = 1;
       it = t3._disques.descendingIterator();
       while (it.hasNext()) {
       	int val = it.next();
       	g2.setColor(new Color((val*100)%255, (val*10000)%255, (val*1000)%255));
           g2.fillRect(x_t3 - val*30, y_base - cpt * h_disque - ((cpt-1) * 5) - 5, val * 60, h_disque);
           cpt++;
       }
		
	}
	private Graphics2D initGraphicContext() {
		Graphics g = this.bufferStrat.getDrawGraphics();
		
		Graphics2D g2 = (Graphics2D) g;
		
		return g2;
	}
	
	private void printContext(Graphics2D g2) {
		bufferStrat.show();
		g2.dispose();
	}
}
